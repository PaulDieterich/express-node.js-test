var express         = require("express");
var bodyParser      = require("body-parser");
var app             = express();

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// index page 
app.get('/', function(req, res) {
    res.render('pages/index');
});
app.post("/", function(req, res){
    var text = request.body.text;
    var name = require.body.name;
    console.log("Text =" + text +", name " + name);
    res.render("pages/index",{text:text , name:name});
});


// about page 
app.get('/about', function(req, res) {
    res.render('pages/about');
});


app.listen(3000, function(){
    console.log("server startet auf port 3000");
});